class CreateHospitals < ActiveRecord::Migration[5.0]
  def change
    create_table :hospitals do |t|
      t.string :mobile_number
      t.integer :state
      t.integer :district
      t.integer :mandal
      t.integer :village

      t.timestamps
    end
  end
end
