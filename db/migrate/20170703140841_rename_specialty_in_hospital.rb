class RenameSpecialtyInHospital < ActiveRecord::Migration[5.0]
  def up
    rename_column :hospitals, :specialty_id, :specialty_ids
    change_column :hospitals, :specialty_ids, :string
  end
  
  def down
    rename_column :hospitals, :specialty, :specialty_id
    change_column :hospitals, :specialty_id, :integer
  end
end
