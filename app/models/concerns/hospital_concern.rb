module HospitalConcern
  extend ActiveSupport::Concern
  include ActiveModel::Validations

  included do
    validates :hospital_name, :presence => true
    validates :mobile_number, :presence => true
    validates :address, :presence => true
    validates :landline_number, :presence => true
    validates :specialty_ids, :presence => true
    # has_many :taggings, as: :taggable
    # has_many :tags, through: :taggings
    STATE = [{ :name => 'AP', :id => '1' }, { :name => 'TG', :id => '2' }]
    DISTRICTS = [{ :name => 'Rangareddy', :id => '1' }, { :name => 'Hyderabad', :id => '2' },
                 { :name => 'Tirupathi', :id => '3' }, { :name => 'East Godhavari', :id => '4' },
                 { :name => 'Ongole', :id => '5' }]

  end

  # methods defined here are going to extend the class, not the instance of it
  module ClassMethods

  end
end