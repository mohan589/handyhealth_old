require 'application_responder'

class ApplicationController < ActionController::Base
  include ActionController::MimeResponds

  self.responder = ApplicationResponder
  respond_to :html

  protect_from_forgery with: :exception
  layout :check_resource

  def check_resource

  end
end
