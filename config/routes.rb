Rails.application.routes.draw do
  resources :specialties
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  resources :hospitals
  resources :home
  root :to  => "home#index"
end
