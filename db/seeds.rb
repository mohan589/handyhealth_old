# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
[
'GENERAL SURGERY',
'ENT SURGERY',
'OPHTHALMOLOGY',
'OBSTETRICS & GYNAECOLOGY',
'ORTHOPAEDICS',
'SURGICAL GASTROENTEROLOGY',
'CARDIO THORASIC SURGERY',
'PAEDIATRIC SURGERY',
'GENITOURINARY SURGERY',
'NEURO SURGERY',
'SURGICAL ONCOLOGY',
'MEDICAL ONCOLOGY',
'RADIATION ONCOLOGY',
'PLASTIC SURGERY',
'POLYTRAUMA',
'COCHLEAR IMPLANT SURGERY',
'PROSTHESES',
'DENTAL SURGERY',
'CRITICAL CARE',
'GENERAL MEDICINE',
'INFECTIOUS DISEASES',
'PAEDIATRICS',
'CARDIOLOGY',
'NEPHROLOGY',
'NEUROLOGY',
'PULMONOLOGY',
'DERMATOLOGY',
'RHEUMATOLOGY',
'ENDOCRINOLOGY',
'MEDICAL GASTROENTEROLOGY'
].each do |surgery|
  Specialty.create(:name => surgery)
end