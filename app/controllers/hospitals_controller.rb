class HospitalsController < ApplicationController
  include SmartListing::Helper::ControllerExtensions
  helper  SmartListing::Helper
  # decorates_assigned :hospital

  before_action :find_pin, only: [:show, :edit, :update, :destroy]
  respond_to :html, :json

  def index
     smart_listing_create(:hospitals, Hospital.all, partial: "hospitals/hospital")
  end

  def show
  end

  def new
    @hospital = Hospital.new()
  end

  def edit
  end

  def create
    @hospital = Hospital.create(hospital_params)
  end

  def destroy
    @hospital.destroy
  end

  def update
    @hospital.update_attributes(hospital_params)
  end

  private

  def hospital_params
    params.require(:hospital).permit(:hospital_name, :mobile_number, :address, :landline_number, :number_of_beds, :specialty_ids => [])
  end

  def find_pin
    @hospital = Hospital.find(params[:id])
  end
end
