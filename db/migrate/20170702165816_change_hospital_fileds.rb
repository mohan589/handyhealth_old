class ChangeHospitalFileds < ActiveRecord::Migration[5.0]
  def change
    rename_column :hospitals, :district, :hospital_name
    change_column :hospitals, :hospital_name, :string
    rename_column :hospitals, :state, :address
    change_column :hospitals, :address, :text
    rename_column :hospitals, :mandal, :landline_number
    change_column :hospitals, :landline_number, :string
    rename_column :hospitals, :village, :number_of_beds
    add_column :hospitals, :specialty_id, :integer
  end
end
